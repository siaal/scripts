#!/bin/sh
# Extract archive file  of a large choice of extensions
for arg in "$@"
do
    if [ -f "$arg" ] ; then
    case "$arg" in
      *.tar.bz2)   tar xjf "$arg"   ;;
      *.tar.gz)    tar xzf "$arg"   ;;
      *.bz2)       bunzip2 "$arg"   ;;
      *.rar)       unrar x "$arg"   ;;
      *.gz)        gunzip "$arg"    ;;
      *.tar)       tar xf "$arg"    ;;
      *.tbz2)      tar xjf "$arg"   ;;
      *.tgz)       tar xzf "$arg"   ;;
      *.zip)       unzip "$arg"     ;;
      *.Z)         uncompress "$arg";;
      *.7z)        7z x "$arg"      ;;
      *.iso)        7z x "$arg"      ;;
      *.deb)       ar x "$arg"      ;;
      *.tar.xz)    tar xf "$arg"    ;;
      *.tar.zst)   unzstd "$arg"    ;;      
      *)           echo "'$arg' cannot be extracted via ex()" ;;
       esac
    echo "Finished extracting $arg"
    else
        echo "'$arg' is not a valid file"
    fi
done


