#!/bin/bash
set -e
set -xv
cmds=("github.com/siaal/go-cmds")

for cmd in "${cmds[@]}"; do
	host="${cmd%%/*}"
	user="${cmd%/*}"
	user="${user#*/}"
	name="${cmd##*/}"
	echo "$cmd"
	echo "$host"
	echo "$user"
	echo "$name"
	cd "$REPOS/"
	mkdir -p "$host" && cd "$host"
	mkdir -p "$user" && cd "$user"
	if ! test -d "$PWD/$name"; then
		git clone "git@${host}:${user}/${name}.git"
	fi
	cd "./$name"
	make install
done
