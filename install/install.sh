#!/bin/sh

set -xv
dir=$(dirname "$(realpath "${0%/*}")")

target="$HOME/.local/bin/"
mkdir -p "$target"
ln -fsv "$dir" "$target"
