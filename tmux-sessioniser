#!/usr/bin/env bash
# Source: https://github.com/ThePrimeagen/.dotfiles/blob/master/bin/.local/scripts/tmux-sessionizer

paths=()
# Append Repos
while  IFS= read -r line; do
	paths+=("$line")
done  <<< "$(find -L "$REPOS" -mindepth 3 -maxdepth 3 -type d)"

# Gets extras from ~/.config/sessions
if test -r "$HOME/.config/sessions"; then
	while IFS= read -r line; do
		export paths+=("$line")
	done < "$HOME/.config/sessions"
fi

unset query
unset args
args=("--tiebreak=end")
# If args - append them to query
test -n "$1" && query="$query $*"
# If <2 args, append gituser
if test -z "$2"; then
	test -n "$GITUSER" && query="$query github.com/$GITUSER"
else
	args+=("-1")
fi

# Append space
test -n "$query" && query="$query "

if test "${#paths[@]}" = "1"; then
	selected="${paths//$HOME/'~'}"
else
	selected="$(
		for item in "${paths[@]}"; do
			echo "${item//$HOME/'~'}"
		done | fzf "${args[@]}" --query "$query"
	)"
fi
test -z "$selected" && exit 0
selected="${selected//'~'/$HOME}"

selected_name=$(echo "$selected" | tr . _)
tmux_running=$(pgrep tmux)

if [[ -z $TMUX ]] && [[ -z $tmux_running ]]; then
	exec tmux new-session -s "$selected_name" -c "$selected" \; split-window -h
fi

if ! tmux has-session -t="$selected_name" 2> /dev/null; then
	tmux new-session -ds "$selected_name" -c "$selected" \; split-window -h
fi

exec tmux switch-client -t "$selected_name"
